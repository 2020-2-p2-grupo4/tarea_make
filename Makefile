CC= gcc -Wall -c

./bin/utilfecha: ./obj/main.o ./obj/segundos.o ./obj/dias.o ./obj/formato.o
	mkdir -p bin/
	gcc $^ -o $@

./obj/main.o: ./src/main.c
	mkdir -p obj/
	$(CC) $^ -I ./head/  -o $@

./obj/segundos.o: ./src/segundos.c
	$(CC) $^ -o $@

./obj/dias.o: ./src/dias.c
	$(CC) $^ -o $@

./obj/formato.o: ./src/formato.c
	$(CC) $^ -o $@

.PHONY: clean
clean:
	rm bin/ -rf  
	rm obj/ -rf 